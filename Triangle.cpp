#include <cstdlib>
#include <string>
#include <iostream>
#include <sstream>
#include "bitmap_image.hpp"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
   float x [3] = {};
   float y [3] = {};
	
   float ac[3] ={};
   float ab[3] ={};
   
   float rgb[3][3] = {};
	
	bitmap_image image(800, 800);
	
	
	
	if (!image)
   {
      printf("Error - Failed to open: pretty.bmp\n");
      return 1;
   }
  
   for(int i = 0; i< 3; i ++){
	std::cout <<"Enter your point " << std::endl;
	string input;
	cin >> input;
	float p1;
	float p2;
	float r;
	float g;
	float b;
	char c, c2, c3, c4, c5;
	std::stringstream ss(input);
	ss >> p1 >> c >> p2 >> c2 >> r >> c3 >> g >> c4 >> b;
	x[i] = p1;
	y[i] =p2;
	rgb[i][0] = r;
	rgb[i][1] = g;
	rgb[i][2] = b;
	std::cout << x[i] << y[i] << rgb[i][0] << rgb[i][1] << rgb[i][2]<< std::endl;
   }
	
	
    for(int i = 0; i < 800; i ++){//x coordinate
		for(int j = 0; j < 800; j++){//y coordinate
			float w1 = ((y[1]-y[2]) * (i - x[2]) + (x[2] - x[1]) * (j - y[2])) / ((y[1]-y[2]) * (x[0]-x[2]) + (x[2] - x[1]) *( y[0] - y[2]));
			float w2 = ((y[2]-y[0]) * (i-x[2]) + (x[0] - x[2]) * (j - y[2])) / ((y[1]-y[2]) * (x[0]-x[2]) + (x[2] - x[1]) *( y[0] - y[2]));
			float w3 = 1 - w1 - w2;
			
			if(w3 < 0 || w2 < 0 || w1 < 0){
				image.set_pixel(i,j,0,0,0);
			}
			else{
				float red = ((w1 * rgb[0][0]) + (w2 * rgb[1][0]) + (w3 * rgb[2][0])) * 255;
				float green = ((w1 * rgb[0][1]) + (w2 * rgb[1][1]) + (w3 * rgb[2][1])) * 255;
				float blue = ((w1 * rgb[0][2]) + (w2 * rgb[1][2]) + (w3 * rgb[2][2])) * 255;
				
				image.set_pixel(i,j,red,(green),(blue));
				
			}
		}
	}

    
    image.save_image("output.bmp");
  
    return 0;
}



